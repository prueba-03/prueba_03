from django.contrib import admin
from app_prueba3.models import Paquete

class PaquetesAdmin(admin.ModelAdmin):
    list_display=("numero_despacho", "nombre_cliente", "direccion")
    search_fields=["numero_despacho"]
    list_filter=["estado"]


# Register your models here.
admin.site.register(Paquete, PaquetesAdmin)