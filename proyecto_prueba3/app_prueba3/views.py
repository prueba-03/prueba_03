from django.http.response import HttpResponseServerError
from django.shortcuts import get_object_or_404, render
from app_prueba3.models import Paquete
from django.contrib import messages


# Create your views here.
def ingresar_paquete(request):
    return render(request, "Crear.html")

def listar_paquetes(request):
    return render(request, "listar.html")

def borrar_paquete(request):
    return render(request, "Eliminar.html")

def modificar_paquete(request):
    return render(request, "Update.html")

def index_html(request):
    return render(request, "index.html")


def ingresar(request):

    numero_despacho = request.GET["txtDespacho"]
    nombre = request.GET["txtCliente"]
    direccion = request.GET["txtDireccion"]
    telefono = request.GET["txtTelefono"]
    producto = request.GET["txtProducto"]
    peso = request.GET["numberPeso"]
    alto =  request.GET["numberAlto"]
    ancho = request.GET["numberAncho"]
    profundidad = request.GET["numberProfundidad"]
    fecha_ingreso = request.GET["dtFechaIngreso"]

    estado = False


    if len(numero_despacho):
        paquete = Paquete(numero_despacho = numero_despacho, 
                    nombre_cliente=nombre, 
                    direccion=direccion, 
                    telefono=telefono, 
                    producto=producto, 
                    peso=peso, 
                    alto_paquete=alto, 
                    ancho_paquete=ancho, 
                    profundida_paquete=profundidad, 
                    fecha_ingreso=fecha_ingreso,
                    estado=estado)

        paquete.save()
        messages.success(request, "Paquete ingresado correctamente")
        return render( request, "Crear.html")
    else:
        return render( request, "Crear.html")



def listar(request):
    paquete = Paquete.objects.all()
    return render(request, "listar.html", {'paquete': paquete})


def borrar(request):
    numero_despacho = request.GET["txtDespacho"]
    if len(numero_despacho)>0:
        paquete_borrar = Paquete.objects.get(numero_despacho = numero_despacho)
        messages.warning(request, "Usuario eliminado correctamente")
        paquete_borrar.delete()
        return render( request, "eliminar.html")
    else:
        return render( request, "eliminar.html")


def borrar2(request, numero_despacho2):
        messages.warning(request, "Usuario eliminado correctamente")
        paquete_borrar = get_object_or_404(Paquete, numero_despacho = numero_despacho2)
        paquete_borrar.delete()
        return render( request, "listar.html")

def modificar(request):
    if request.GET["txtDespacho"]:
        numero_despacho = request.GET["txtDespacho"]
        fecha_envio = request.GET["dtFechaEnvio"]
        estado_recibido = True
        paquete = Paquete.objects.filter(numero_despacho=numero_despacho)
        if paquete:
            paq = Paquete.objects.get(numero_despacho=numero_despacho)
            paq.fecha_envio = fecha_envio
            paq.estado = estado_recibido
            paq.save()     
            messages.success(request, "Paquete modificado correctamente")    
            #mensaje de sweetalet
            return render( request, "Update.html")
        else:
            return render( request, "Update.html")
    